# chirpstack

![Version: 0.14.0](https://img.shields.io/badge/Version-0.14.0-informational?style=flat-square) ![AppVersion: 3.x](https://img.shields.io/badge/AppVersion-3.x-informational?style=flat-square)

Community Helm Chart for chirpstack.io network stack.
This directory contains a Kubernetes chart to deploy a [Chirpstack](https://chirpstack.io) cluster using deployments, services and istio if enabled.

## Prerequisites Details
* Kubernetes 1.15+
* Istio if enabled
* Prometheus-Operator if enabled
* Postgresql
* Redis
* MQTT Broker

## Chart Details
This chart will do the following:

* Deploy a network-server
* Deploy an application-server
* (Experimental) Deploy gateway-bridge

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| appserver.api.external | object | `{"existingSecret":"chirpstack-jwt-secret"}` | REST-API |
| appserver.api.external.existingSecret | string | `"chirpstack-jwt-secret"` | secret is required key(signSecret) |
| appserver.api.public_host | string | `""` | leave empty for internal, external grpc not yet supported |
| appserver.codec.js.maxExecutionTime | string | `"100ms"` |  |
| appserver.enabled | bool | `true` |  |
| appserver.general.GRPCDefaultResolverScheme | string | `"passthrough"` |  |
| appserver.general.logLevel | int | `4` |  |
| appserver.image.pullPolicy | string | `"IfNotPresent"` |  |
| appserver.image.repository | string | `"chirpstack/chirpstack-application-server"` |  |
| appserver.image.tag | string | `"3.17.0"` |  |
| appserver.integration.kafka.algorithm | string | `"SHA-512"` | only used if mechanism: scram |
| appserver.integration.kafka.brokers[0] | string | `"chirpstack-kafka-bootstrap:9092"` |  |
| appserver.integration.kafka.enabled | bool | `false` |  |
| appserver.integration.kafka.existingSecret | string | `""` |  |
| appserver.integration.kafka.existingSecretPasswordKey | string | `"password"` |  |
| appserver.integration.kafka.mechanism | string | `"plain"` | plain or scram |
| appserver.integration.kafka.tls | bool | `false` |  |
| appserver.integration.kafka.topic | string | `"chirpstack_as"` |  |
| appserver.integration.kafka.username | string | `""` |  |
| appserver.integration.mqtt.enabled | bool | `true` |  |
| appserver.integration.mqtt.existingSecret | string | `""` | secret and keys are optional |
| appserver.integration.mqtt.server | string | `"tcp://mqtt-mosquitto:1883"` |  |
| appserver.istio.enabled | bool | `false` |  |
| appserver.istio.gateway.cors.additionalOrigins | list | `[]` |  |
| appserver.istio.gateway.cors.enabled | bool | `true` | https://$HOST if tls enabled |
| appserver.istio.gateway.host | string | `"localhost"` |  |
| appserver.istio.gateway.selector | string | `""` | ingressgateway |
| appserver.istio.gateway.tls.credentialName | string | `"CHANGE_ME"` |  |
| appserver.istio.gateway.tls.enabled | bool | `true` |  |
| appserver.marshaler | string | `"json_v3"` | v3 JSON (will be removed in the next major chirpstack release) |
| appserver.monitoring.perDeviceEventLogMaxHistory | int | `10` | 0 When set to > 0, ChirpStack Application Server will log events per device |
| appserver.name | string | `"as"` |  |
| appserver.postgres.dbname | string | `"chirpas"` |  |
| appserver.postgres.existingSecret | string | `"CHANGE_ME"` |  |
| appserver.postgres.host | string | `"chirpstack-postgres"` |  |
| appserver.postgres.port | int | `5432` |  |
| appserver.postgres.sslmode | string | `"require"` |  |
| appserver.prometheus.enabled | bool | `false` |  |
| appserver.prometheus.interval | string | `"60s"` |  |
| appserver.prometheus.namespace | string | `"monitoring"` |  |
| appserver.redis.cluster | bool | `false` |  |
| appserver.redis.existingSecret | string | `""` |  |
| appserver.redis.existingSecretPasswordKey | string | `"redis-password"` |  |
| appserver.redis.master | string | `""` |  |
| appserver.redis.servers[0] | string | `"redis-chirpstack-redis:6379"` |  |
| appserver.replicas | int | `1` |  |
| appserver.service.type | string | `"ClusterIP"` |  |
| common.dns | string | `"cluster.local"` |  |
| gatewaybridge.enabled | bool | `false` |  |
| gatewaybridge.image.pullPolicy | string | `"IfNotPresent"` |  |
| gatewaybridge.image.repository | string | `"chirpstack/chirpstack-gateway-bridge"` |  |
| gatewaybridge.image.tag | string | `"3.9.2"` |  |
| gatewaybridge.name | string | `"gb"` |  |
| gatewaybridge.replicas | int | `1` |  |
| gatewaybridge.service.type | string | `"ClusterIP"` |  |
| networkserver.enabled | bool | `true` |  |
| networkserver.gateway.backend.mqtt.event_topic | string | `"gateway/+/event/+"` |  |
| networkserver.gateway.backend.mqtt.existingSecret | string | `""` | secret and keys are optional |
| networkserver.gateway.backend.mqtt.server | string | `"tcp://mqtt-mosquitto:1883"` |  |
| networkserver.gateway.backend.type | string | `"mqtt"` | only mqtt supported right now |
| networkserver.general.GRPCDefaultResolverScheme | string | `"passthrough"` | Set this to "dns" for enabling dns round-robin load balancing. |
| networkserver.general.logLevel | int | `4` | debug=5, info=4, warning=3, error=2, fatal=1, panic=0 |
| networkserver.image.pullPolicy | string | `"IfNotPresent"` |  |
| networkserver.image.repository | string | `"chirpstack/chirpstack-network-server"` |  |
| networkserver.image.tag | string | `"3.15.0"` |  |
| networkserver.join.default.server | string | `""` | defaults to app-server(enabled) if empty |
| networkserver.monitoring.deviceFrameLogMaxHistory | int | `0` | When set to a value > 0, ChirpStack Network Server will log all uplink and downlink frames associated to a device as a Redis stream  |
| networkserver.monitoring.gatewayFrameLogMaxHistory | int | `0` | When set to a value > 0, ChirpStack Network Server will log all uplink and downlink frames received by the gateways as a Redis stream |
| networkserver.monitoring.perDeviceFrameLogMaxHistory | int | `10` | Equal to the device_frame_log_max_history, but it has the device DevEUI in the Redis key. This feature is used by the ChirpStack Application Server web-interface. |
| networkserver.monitoring.perGatewayFrameLogMaxHistory | int | `10` | Equal to the gateway_frame_log_max_history, but it has the GatewayID in the Redis key. This feature is used by the ChirpStack Application Server web-interface. |
| networkserver.name | string | `"ns"` |  |
| networkserver.network.band.name | string | `"EU868"` |  |
| networkserver.network.id | string | `"000000"` | Network identifier https://lora-alliance.org/sites/default/files/2018-11/20181114_NetID_FAQ.pdf |
| networkserver.network.settings.extra_channels | object | `{"channels":"[[network_server.network_settings.extra_channels]]\nfrequency=867100000\nmin_dr=0\nmax_dr=5\n\n[[network_server.network_settings.extra_channels]]\nfrequency=867300000\nmin_dr=0\nmax_dr=5\n\n[[network_server.network_settings.extra_channels]]\nfrequency=867500000\nmin_dr=0\nmax_dr=5\n\n[[network_server.network_settings.extra_channels]]\nfrequency=867700000\nmin_dr=0\nmax_dr=5\n\n[[network_server.network_settings.extra_channels]]\nfrequency=867900000\nmin_dr=0\nmax_dr=5\n","enabled":false}` | https://lora-alliance.org/sites/default/files/2018-04/lorawantm_regional_parameters_v1.1rb_-_final.pdf |
| networkserver.network.settings.installationMargin | int | `10` |  |
| networkserver.network.settings.rejoinRequest.enabled | bool | `false` |  |
| networkserver.network.settings.rejoinRequest.maxCountN | int | `0` | The device must send a rejoin-request type 0 at least every 2^(max_count_n + 4) uplink messages 0<=n<=15 |
| networkserver.network.settings.rejoinRequest.maxTimeN | int | `0` | The device must send a rejoin-request type 0 at least every 2^(max_time_n + 10) secs  0<=n<=15 |
| networkserver.network.settings.rx2PreferOnLinkBudget | bool | `false` |  |
| networkserver.network.settings.uplink_channels | list | `[]` | when left blank, all channels will be enabled. |
| networkserver.postgres.dbname | string | `"chirpns"` |  |
| networkserver.postgres.existingSecret | string | `"CHANGE_ME"` | secret must set and contain keys username and password |
| networkserver.postgres.host | string | `"chirpstack-postgres"` |  |
| networkserver.postgres.port | int | `5432` |  |
| networkserver.postgres.sslmode | string | `"require"` |  |
| networkserver.prometheus.enabled | bool | `false` |  |
| networkserver.prometheus.interval | string | `"60s"` |  |
| networkserver.prometheus.namespace | string | `"monitoring"` |  |
| networkserver.redis.cluster | bool | `false` |  |
| networkserver.redis.existingSecret | string | `""` |  |
| networkserver.redis.existingSecretPasswordKey | string | `"redis-password"` |  |
| networkserver.redis.master | string | `""` |  |
| networkserver.redis.servers[0] | string | `"redis-chirpstack-redis:6379"` |  |
| networkserver.replicas | int | `1` |  |
| networkserver.service.type | string | `"ClusterIP"` |  |


## Secrets

```yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: chirpstack-as-postgresql # required
type: Opaque
data:
  username: #base64
  password: #base64

---
apiVersion: v1
kind: Secret
metadata:
  name: chirpstack-as-redis # optional
type: Opaque
data:
  password: #base64

---
apiVersion: v1
kind: Secret
metadata:
  name: chirpstack-as-mqtt # optional
type: Opaque
data:
  username: #base64
  password: #base64

---
apiVersion: v1
kind: Secret
metadata:
  name: kafka-credentials # optional
type: Opaque
data:
  password: #base64

---
apiVersion: v1
kind: Secret
metadata:
  name: chirpstack-as-signSecret # required
type: Opaque
data:
  signSecret: #base64

---
apiVersion: v1
kind: Secret
metadata:
  namespace: istio-system # Same namespace where the ingress-gateway is located
type: kubernetes.io/tls
data:
  ca.crt: ""
  tls.crt: ""
  tls.key: ""

---
apiVersion: v1
kind: Secret
metadata:
  name: chirpstack-ns-postgresql # required
type: Opaque
data:
  username: #base64
  password: #base64

---
apiVersion: v1
kind: Secret
metadata:
  name: chirpstack-ns-redis # optional
type: Opaque
data:
  password: #base64

---
apiVersion: v1
kind: Secret
metadata:
  name: chirpstack-ns-mqtt # optional
type: Opaque
data:
  username: #base64
  password: #base64

```

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.5.0](https://github.com/norwoodj/helm-docs/releases/v1.5.0)
